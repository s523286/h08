package com.spnotes.kafka.simple;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.Scanner;
import java.util.Random;

/**
 * Created by Liz Conard on 10/22/2018.
 */
public class Producer {
    private static Scanner in;
    private static int a;
    public static void main(String[] argv)throws Exception {
        if (argv.length != 1) {
            System.err.println("Please specify 1 parameters ");
            System.exit(-1);
        }
        String topicName = argv[0];
        in = new Scanner(System.in);
        // System.out.println("Enter message(type exit to quit)");

        //Configure the Producer
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArraySerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");
        org.apache.kafka.clients.producer.Producer producer = new KafkaProducer(configProperties);
//        Random random = new Random();
            String a;
            System.out.println("BINGO Number Generator");
            System.out.println("=====================================");
            System.out.println("Enter 'ok' to generate a random BINGO number");
            a = in.nextLine();
           
            String[] bingo = new String[] {
                "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12", "B13", "B14", "B15",
				"I16", "I17",	"I18",	"I19",	"I20",	"I21",	"I22",	"I23",	"I24",	"I25",	"I26",	"I27",	"I28",	"I29",	"I30",
				"N31",	"N32",	"N33",	"N34",	"N35",	"N36",	"N37",	"N38",	"N39",	"N40",	"N41",	"N42",	"N43",	"N44",	"N45",
				"G46",	"G47",	"G48",	"G49",	"G50",	"G51",	"G52",	"G53",	"G54",	"G55",	"G56",	"G57",	"G58",	"G59",	"G60",
				"O61",	"O62",	"O63",	"O64",	"O65",	"O66",	"O67",	"O68",	"O69",	"O70",	"O71",	"O72",	"O73",	"O74",	"O75"
            };
//        String line = in.nextLine();
//        for(int i = 0; i < 20; i++) {
            // Pick a sentence at random
            //String s = sentences[random.nextInt(sentences.length)];
            // Send the sentence to the test topic

            if(a == "ok"){
				//Random random = new Random();
				//int rnum = random.nextInt(75)+1;
				String s = bingo[5];
				ProducerRecord<String, String> rec = new ProducerRecord<String, String>(topicName, s);
				producer.send(rec);
				System.out.println("Enter 'ok' to generate a random BINGO number");
				System.out.println("Enter 'stop' to quit");
				a = in.nextLine();
			}
//        }
        in.close();
        producer.close();
    }
}
